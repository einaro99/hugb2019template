import PySimpleGUI as sg

layout = [
    [sg.Text("Username"), sg.Input(key="username")],
    [sg.Text("Email"), sg.Input(key="email", pad=((31,0), (0,0)))],
    [sg.Text("Password"), sg.Input(key="password", password_char="*", pad=((7,0), (0,0)))],
    [sg.Button("Log in", key="submit")]
    ]

window = sg.Window("User Login", layout)


def new_window():
    layout = \
    [
    [sg.Image("dog.png")],
    [sg.Text("Walk this dog?"), sg.Button("Walk dog", key="btn")]
    ]
    return sg.Window("Dog", layout)

while True:
    event, values = window.read()
    if event == "submit":
        if values["username"] == "" or values["email"] == "" or values["password"] == "":
            sg.popup("Please enter values")
        else:
            sg.popup("Welcome back", values["username"])
            window.Close()
            window = new_window()
    
    if event == "btn":
        sg.popup("Thank you for walking")
        break