#An easy WebSockets client fitting our server code
import asyncio
import websockets
import json

#This is pretty much the same method as in the basic example,
# just modified with JSON formatting and asking for an operation instead of a name
async def msgSender():
    #Our server address (here: running on the same computer at port 8080)
    uri = "ws://localhost:8080"
    #Connect to the server
    async with websockets.connect(uri) as websocket:
        #Ask the user for a method to run (method name)
        print("=================================")
        print("Welcome to our gardening service!")
        print("=================================")
        print("Available inputs:\n\tcreate_post\n\tedit_post\n\tget_user\n\tget_user_posts\n\tdelete_user\n\tcreate_user\n\tedit_user\n\tget_post_by_id\n\tdelete_post\n\tcreate_comment\n\tcreate_private_message\n\tdelete_comment\n\tedit_comment ")

        op = input("Enter name of function: ")
        if op == "create_post" or op == "edit_post":
            message = input("Message: ")
            tpe = input("Type: ")
            username = input("Username: ")
            if op == "edit_post":
                post_id = input("Post id: ")
                await websocket.send(json.dumps({"op":op, "message":message, "id":post_id, "tpe":tpe, "username":username}))
            await websocket.send(json.dumps({"op":op, "message":message, "tpe":tpe, "username":username}))
        elif op == "get_user" or op == "get_user_posts" or op == "delete_user":
            username = input("Enter username: ")
            await websocket.send(json.dumps({"op":op, "username":username}))
        elif op == "create_user":
            first_name = input("First Name: ")
            last_name = input("Last Name: ")
            phone = input("Phone Number: ")
            username = input("Username: ")
            password = input("Password: ")
            email = input("Email: ")
            await websocket.send(json.dumps({"op":op, "first_name":first_name, "last_name":last_name, "phone":phone,
                                            "username":username, "password":password, "email":email}))
        elif op == "edit_user":
            print("Enter nothing if you don't wish to edit.")
            username = input("Enter Username: ")
            new_phone = input("New Phone Number: ")
            new_username = input("New Username: ")
            new_email = input("New Email: ")
            new_password = input("New Password: ")
            await websocket.send(json.dumps({"op":op, "username":username, "new_phone":new_phone, "new_username":new_username,
                                            "new_email":new_email, "new_password":new_password}))
        elif op == "get_post_by_id" or op == "delete_post":
            _id = input("Enter id: ")
            await websocket.send(json.dumps({"op":op, "id":_id}))
        elif op in ["create_comment", "create_private_message", "edit_comment"]:
            username = input("Enter Username: ")
            post_id = input("Post id: ")
            message = input("Comment here: ")
            if op == "edit_comment":
                comment_id = input("Comment id: ")
                await websocket.send(json.dumps({"op":op, "comment_id":comment_id, "message":message, "post_id":post_id, "username":username }))
            await websocket.send(json.dumps({"op":op, "message":message, "post_id":post_id, "username":username }))
        elif op == "delete_comment":
            comment_id = input("Comment id: ")
            await websocket.send(json.dumps({"op":op, "id":comment_id}))      
        else:
            await websocket.send(json.dumps({"op":op}))
        #Encode the method name into a JSON string
        # (we know that the server expects a JSON string with field 'op' containing the method name)


        #Just wait for a reply and print the return value (a JSON string)
        return_val = await websocket.recv()
        print(f"< {return_val}")

#This is essentially the same as above, just instead of a user input we expect a parameter
async def auto_msg(op):
    uri = "ws://localhost:8080"
    async with websockets.connect(uri) as websocket:
        await websocket.send(json.dumps({"op":op}))

        return_val = await websocket.recv()
        #Also, instead of printing the result we return it
        return return_val

if __name__ == '__main__':
    #Run the msgSender() function until it is complete
    #This means it is run once only; the program ends afterwards
    #You could also loop this execution, like on the server (e.g., if you had a chat program)
    for i in range(10):
        asyncio.get_event_loop().run_until_complete(msgSender())
