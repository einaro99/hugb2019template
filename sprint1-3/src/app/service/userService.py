from sys import path
path.append(".")

from src.app.repositories.userRepository import UserRepository
from src.app.service.postService import PostService
from src.app.service.commentService import CommentService
import json

#TODO Create get_user_posts

class User:
    def __init__(self):
        self.userService = UserService()

    def create_user(self, first_name, last_name, phone,
                    username, password, email, isAdmin=False):
        return self.userService.create_user(first_name, last_name,
                phone, username, password, email, isAdmin)

    def get_user(self, username):
        return self.userService.get_user(username)

    def get_all_users(self):
        return self.userService.get_all_users()

    def delete_user(self, username):
        return self.userService.delete_user(username)

    def edit_user(self, username, new_phone=None, new_username=None,
                  new_email=None, new_password=None):
        return self.userService.edit_user(username, new_phone,
                new_username, new_email, new_password)

class UserService:
    def __init__(self):
        self.userRepo = UserRepository()
        self.all_users = self.userRepo.get_users()
        self.first_name = ""
        self.last_name = ""
        self.phone = ""
        self.username = ""
        self.password = ""
        self.email = ""
        self.isAdmin= ""

    def username_exists(self, username):
        for user in self.all_users:
            if user["username"] == username:
                return True
        return False

    def is_admin(self, username):
        for user in self.all_users:
            if user["username"] == username and user["isAdmin"] == True:
                return True
        return False    

    def create_user(self, first_name, last_name, phone,
                    username, password, email, isAdmin=False):
        print(email)
        if self.username_exists(username):
            return "Username already taken"
        if first_name != "" and last_name != "":
            if phone != "":
                if username != "" and password != "":
                    if email != "":
                        new_user = {
                            "first_name": first_name,
                            "last_name": last_name,
                            "phone": phone,
                            "username": username,
                            "password": password,
                            "email": email,
                            "isAdmin": isAdmin
                        }
                        self.userRepo.create_user(new_user)
                        return "User registered"
                    return "Invalid Email"
                return "Invalid Username or Password"
            return "Invalid Phone Number"
        return "Invalid First or Last Name"

    def get_user(self, username):
        for user in self.all_users:
            if user["username"] == username:
                return user
        return None

    def get_all_users(self):
        return self.all_users

    def delete_user(self, username):
        index = 0
        for user in self.all_users:
            if user["username"] == username:
                self.userRepo.delete_user(index)
                return "User deleted"
            index += 1
        return "User not found"

    def edit_user(self, username, new_phone="", new_username="",
                  new_email="", new_password=""):
        if not self.username_exists(username):
            return "User does not exist"
        if self.username_exists(new_username):
            return "New Username already taken"
        for user in self.all_users:
            if user["username"] == username:
                if new_phone == "":
                    new_phone = user["phone"]
                elif new_username == "":
                    new_username = user["username"]
                elif new_email == "":
                    new_email = user["email"]
                elif new_password == "":
                    new_password = user["password"]
                user["phone"] = new_phone
                user["username"] = new_username
                user["email"] = new_email
                user["password"] = new_password
                self.userRepo.edit_user(self.all_users)
                return "User updated"
    #How this works: Service takes the input from the websocket and
    #then sends the input into the applicable repo where it is then
    #added into the corresponding json file