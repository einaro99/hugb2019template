from sys import path
path.append(".")

from src.app.repositories.postRepository import PostRepository
from src.app.repositories.userRepository import UserRepository
from src.app.repositories.commentRepository import CommentRepository
import uuid


class Post:
    def __init__(self):
        self.post_service = PostService()

    def collect_posts(self):
        return self.post_service.get_all_posts()

    def collect_user_post(self, username):
        return self.post_service.get_user_posts(username)

    def collect_post_by_id(self, id):
        return self.post_service.get_post_id(id)

    def create_post(self, message, tpe, username):
        return self.post_service.add_post(message,tpe, username)
    
    def delete_post(self, post_id):
        return self.post_service.delete_post(post_id)
    
    def edit_post(self, message, post_id, tpe, username):
        return self.post_service.edit_post(message, post_id, tpe, username)

class PostService:
    def __init__(self):
        self.__postRepo = PostRepository()
        self.__commentRepo = CommentRepository()
        self.__userRepo = UserRepository()
        self.all_posts = self.__postRepo.get_posts()
        self.all_comments = self.__commentRepo.get_comments()
        self.all_users = self.__userRepo.get_users()
        self.size = len(self.all_posts)

    def get_all_posts(self):
        return self.all_posts

    def get_user_posts(self, username):
        ret_lis = []
        for posts in self.all_posts:
            if posts["username"] == username:
                ret_lis.append(posts)
        return ret_lis


    def get_post_id(self, post_id): #Þarf þetta að returna lista?
        for posts in self.all_posts:
            if posts["id"] == post_id:
                return posts
        

    def add_post(self, message, tpe, username):
        post_id = str(uuid.uuid4())
        if message != "":
            if tpe == "Offer" or tpe == "Request":
                if self.validate_user(username):
                    new_post = {
                        "id":post_id,
                        "message": message,
                        "type": tpe,
                        "username": username
                        }
                    self.__postRepo.create_post_data(new_post)
                    return "Post created"
                return "invalid username"
            return "invalid type"
        return "invalid message"
                
    def delete_post(self, post_id):
        index = 0
        for posts in self.all_posts:
            if posts["id"] == post_id:
                self.__postRepo.delete_post_data(index)
                return "Post deleted"
            index += 1
        return "Post not found"

    def edit_post(self, message="", post_id= "", tpe="", username=""):
        for posts in self.all_posts:
            if posts["id"] == post_id:
                if message == "":
                    message = posts["message"]
                if tpe != "Request" or tpe != "Offer":
                    tpe = posts["type"]
                if username == "":
                    username = posts["username"]
                if self.validate_user(username):
                    posts["id"] = post_id
                    posts["message"] = message
                    posts["type"] = tpe
                    posts["username"] = username
                    self.__postRepo.edit_post_data(self.all_posts)
                    return "Post updated"
                return "User not found"
        return "post not found"


    def validate_user(self, username):
        for users in self.all_users:
            if users["username"] == username:
                return True
        return False

    #def add_post(self, new_message, new_type= "Request"):
    #    post_info = [ str(uuid.uuid4()), new_message, new_type,self.user]
    #    self.all_posts.append(post_info)
    #    self.__postRepo.write_posts()
#
    #def remove_post(self, post_ID):
    #    index = 0
    #    for posts in self.all_posts:
    #        if posts[3] == self.user:
    #            if posts[0] == post_ID:
    #                self.all_posts.pop(index)
    #        index += 1
    #    self.remove_comments_from_post(post_ID)
    #    self.__postRepo.write_posts()
    #
    #
    #def remove_all_posts(self):
    #    index = 0
    #    for posts in self.all_posts:
    #        if posts[3] == self.user:
    #            post_id = posts[0]
    #            self.all_posts.pop(index)
    #        index += 1
    #        self.remove_comments_from_post(post_id)
    #    self.__postRepo.write_posts()
#
    #def remove_comments_from_post(self, post_ID):
    #    self.all_comments = self.__commentRepo.get_comments()
    #    index = 0
    #    print("")
    #    print(self.all_comments)
    #    print("")
    #    for comments in self.all_comments:
#
    #        if comments[1] == post_ID:
    #            self.all_comments.pop(index)
    #        index += 1
    #        self.__commentRepo.write_comments()
#

#
    #def get_user(self, post_id):
    #    for posts in self.all_posts:
    #        if posts[0] == post_id:
    #            for users in self._all_users:
    #                if posts[3] == users[3]:
    #                    return users
#
    #def edit_post(self, new_message, new_type, post_id):
    #    post = self.get_post(post_id)
    #    if post[3] == self.user:
    #        if new_message != None: #Checks if the new_message is empty
    #            post[1] = new_message #Updates the post_message
    #        if new_type == "Request" or new_type == "Offer": #Checks if the new_type is allowed
    #            post[2] = new_type  #then it updates the post_type
    #    self.__postRepo.write_allposts(self.all_posts)
#

#rikki = PostService("rikki")
#nikki = PostService("nikki")
#nikki.add_post("Slátturvél")
#nikki.add_post("Orf")
#rikki.add_post("Slátturvél")
#rikki.add_post("Garðklippur")
#rikki.add_post("Mold")
##rikki.remove_post("6192f55c-fd0c-411b-a56d-5fc301f7e881")