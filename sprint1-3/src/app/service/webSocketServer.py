from sys import path
path.append(".")


#This example includes some more advanced concepts.
#To start with a basic websocket implementation, try to get the basic example from
#the original documentation to run, as we did in L7:
#https://websockets.readthedocs.io/en/stable/intro.html

#asyncio is used to ensure that we can do asynchronous operations (non-blocking)
import asyncio
import json
import websockets

#We need our station code, to have actual "functionality"
#This represents our actual system.
#The code in this file represents the interface - the connection to the outside
from src.app.service.userService import User
from src.app.service.postService import Post
from src.app.service.commentService import Comment
from src.app.service.commentService import PrivateComment

#This is our actual interface - the methods we expose to the outside
class ComponentInterface:
    def __init__ (self):
        self.user = User()
        self.post = Post()
        self.comment = Comment()
        self.privatemessage = PrivateComment()

    def create_user(self, first_name, last_name, phone,
                    username, password, email, isAdmin=False):
        return self.user.create_user(first_name, last_name,
                phone, username, password, email, isAdmin)

    def get_user(self, username):
        return self.user.get_user(username)

    def get_all_users(self):
        return self.user.get_all_users()

    def delete_user(self, username):
        return self.user.delete_user(username)

    def edit_user(self, username, new_phone="", new_username="", new_email="", new_password=""):
        return self.user.edit_user(username, new_phone, new_username, new_email, new_password)

    def get_posts(self):
        return self.post.collect_posts()

    def get_user_posts(self, username):
        return self.post.collect_user_post(username)

    def get_post_by_id(self, id):
        return self.post.collect_post_by_id(id)

    def create_post(self, message, tpe, username):
        return self.post.create_post(message, tpe, username)

    def delete_post(self, post_id):
        return self.post.delete_post(post_id)

    def edit_post(self, message, post_id, tpe, username):
        return self.post.edit_post(message, post_id, tpe, username)
    
    def create_comment(self, message, post_id, username):
        return self.comment.create(message, post_id, username)
    
    def create_private_message(self, message, post_id, username):
        return self.privatemessage.create(message, post_id, username)

    def edit_comment(self, comment_id, message, post_id, username):
        return self.comment.edit(comment_id, message, post_id, username)

    def delete_comment(self, comment_id):
        return self.comment.delete(comment_id)
    #def reportStatus (self):
    #    return self.station1.reportStatus()

#This class represents our technical connection to the outside.
#It allows processing of incoming text messages via WebSockets
#It is implemented as a Singleton pattern (which ensures there is only ever one instance of this class)
#Details on Singleton in the design lectures and here: https://en.wikipedia.org/wiki/Singleton_pattern
class ComponentPort:
    #This is our (private) instance
    __instance = None

    #This is a class method - it can be called without an instance existing
    @staticmethod
    def get_instance():
        """ Static access method. """
        if ComponentPort.__instance == None:
            #in this particular case, the method makes sure that an instance is created if none exists
            ComponentPort()

        #afterwards (or if an instance did already exist), we return the instance
        return ComponentPort.__instance

    #The constructor throws an exception if we already have an instance - we don't want to allow any other instances
    #Note: This is actually not a nice implementation in Python. In other languages, you can actually force that only one instance is created.
    #In Python, this is more of a convention...
    def __init__(self):
        if ComponentPort.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            #If there is no instance, we create one
            ComponentPort.__instance = self
            self.iface = ComponentInterface()

    #Up to here, all the code was just Singleton stuff.
    #This is the real WebSockets meat
    async def __msg_handler(self, websocket, path):
        #We wait until we receive a message (from a client/other system)

        msg = await websocket.recv()
        #We assume that the received msg is JSON and convert it to an object
        #Note that in a real system you want to have some exception handling here...
        data = json.loads(msg)

        #We have decided (design decision) that we expect a JSON string that has an 'op' field.
        #This op field contains the interface method we want to call
        if "op" in data:
            try:
                print("trying to call %s"%(data["op"]))
                #This might be cryptic: we try to call the method in our ComponentInterface instance
                #that has the same name as data["op"].
                #You could also have multiple if/else statements here that check for the values of "op"



                if "username" in data:
                    if "first_name" in data:
                        if "last_name"in data:
                            if "phone" in data:
                                if "password" in data:
                                    print(data)
                                    op_return = getattr(self.iface, data["op"])(data["first_name"],
                                                            data["last_name"],data["phone"],data["username"],
                                                            data["password"], data["email"])
                    elif "new_phone" in data:
                        if "new_username" in data:
                            if "new_email" in data:
                                if "new_password" in data:
                                    op_return = getattr(self.iface, data["op"])(data["username"],
                                                            data["new_phone"],data["new_username"],
                                                            data["new_email"], data["new_password"])

                    elif "message" in data:
                        if "tpe" in data:
                            if "id" in data:
                                op_return = getattr(self.iface, data["op"])(data["message"],
                                                    data["id"], data["tpe"], data["username"])

                            else:
                                op_return = getattr(self.iface, data["op"])(data["message"],
                                                    data["tpe"], data["username"])

                        elif "post_id" in data:
                            if "comment_id" in data:
                                op_return = getattr(self.iface, data["op"])(data["comment_id"], data["message"], 
                                                    data["post_id"], data["username"])
                            else:
                                op_return = getattr(self.iface, data["op"])(data["message"], 
                                                    data["post_id"], data["username"])
                    else:
                        op_return = getattr(self.iface, data["op"])(data["username"])

                elif "id" in data:
                    op_return = getattr(self.iface, data["op"])(data["id"])

                elif "comment_id" in data:
                    op_return = getattr(self.iface, data["op"])(data["comment_id"])
                else:
                    op_return = getattr(self.iface, data["op"])()
                #we take whatever our function call returns and convert it to json
                return_value = json.dumps({"msg":op_return})

                #
                # In this example, all our interface methods have no parameters.
                # In your case, you will have parameters (e.g., a username if you want to create a new user)
                # You will have to put these paramteres also into the JSON string
                # E.g., for the "create user" scenario, you probably will have something checking whether data["username"] exists, then put that into the method as a param
                #

            except AttributeError:
                #if we cannot call the method (Line 80 above), we return an error (in form of a JSON string message)
                return_value = json.dumps({"msg":"Method %s not implemented"%(data["op"])})
                print()

        else:
            #if there is no "op" field in the JSON string, we return an error (again as a JSON string message)
            return_value = json.dumps({"msg":"Invalid data format"})

        #We return our return message (depending on the outcome a different JSON string)
        await websocket.send(return_value)

    #This logic starts the websocket server and listens until we kill the application
    #In the basic example, this code is just below the actual method
    #Here, I placed it into a function so that I can just call it below in the main part (lines 114-117)
    def start(self):
        #Note here that __msg_handler is the name of the function that should be called whenever a message arrives
        #That's the function defined above (Line 65)
        start_server = websockets.serve(self.__msg_handler, "0.0.0.0", 8080)

        #Run until forever
        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

if __name__ == '__main__':
    #Here we get out ComponentPort Singleton (our single instance) and start the WebSockets server
    compPort = ComponentPort.get_instance()
    compPort.start()
