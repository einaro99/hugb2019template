from sys import path
path.append(".")

import uuid
from src.app.repositories.commentRepository import CommentRepository
from src.app.service.postService import PostService
from src.app.service.postService import Post
from src.app.repositories.userRepository import UserRepository
from src.app.repositories.postRepository import PostRepository

class Comment:
    def __init__(self):
        self.comment_service = CommentService(False)

    def create(self, message, post_id, username):
        return self.comment_service.create_comment(message, post_id, username)
    def delete(self, comment_id):
        return self.comment_service.delete_comment(comment_id)
    def get(self, comment_id):
        return self.comment_service.get_comment(comment_id)
    def get_all(self):
        return self.comment_service.get_all_comments()

    def get_post_comments(self, post_id):
        return self.comment_service.get_post_comments(post_id)

    def edit(self, comment_id, message, post_id, username):
        return self.comment_service.edit_comment(comment_id, message)

class PrivateComment:
    def __init__(self):
        self.comment_service = CommentService(True)

    def create(self, message, post_id, username):
        return self.comment_service.create_comment(message, post_id, username)
    # def get(self, comment_id):
    #     return self.comment_service.get_comment(comment_id)
    # def get_all(self):
    #     return self.comment_service.get_all_comments()

    def get_post_comments(self, post_id, username, postcreator):
        return self.comment_service.get_priv_post_comments(post_id, username, postcreator)
    
    

class CommentService:
    def __init__(self, ispriv=False):
        self.ispriv = ispriv
        if ispriv:
            self.commentRepo = PrivateCommentRepository()
            self.all_comments = self.commentRepo.get_comments()
        else:
            self.commentRepo = CommentRepository()
            self.all_comments = self.commentRepo.get_comments()
        self.postServ = PostService()
        self.userRepo = UserRepository()
        self.postRepo = PostRepository()
        self.post = Post()
        

    def edit_comment(self, comment_id, message):
        for comments in self.all_comments:
            if comments["id"] == comment_id:
                if message == "":
                    message = comments["message"]
                    comments["id"] = comment_id
                    comments["message"] = message
                    #self.commentsrepo.editcomments(self.all_comments)
                    return "Comment updated"
        return "Comment not found"


    def get_comment(self, comment_id):
        for comment in self.all_comments:
            if comment["id"] == comment_id:
                return comment

    def get_post_comments(self, post_id):
        ret_lis = []
        for comments in self.all_comments:
            if comments["post_id"] == post_id:
                ret_lis.append(comments)
        return ret_lis
    
    def get_priv_post_comments(self, post_id, username, postcreator):
        ret_lis = []
        for comments in self.all_comments:
            if comments["post_id"] == post_id:
                if comments["username"] == username or comments["username"] == postcreator:
                    ret_lis.append(comments)
        return ret_lis

    def get_all_comments(self):
        return self.all_comments

    def delete_comment(self, comment_id):
        index = 0
        for comments in self.all_comments:
            if comments["id"] == comment_id:
                self.all_comments = self.commentRepo.delete_comment(index)
                
                return "comment deleted"
            index += 1
        return "Comment not found"

    def create_comment(self, message, post_id, username):
        comment_id = str(uuid.uuid4())
        if message != "":
            if self.__validate_post(post_id):
                if self.__validate_user(username):
                    new_comment = {
                        "id": comment_id,
                        "message": message,
                        "post_id": post_id,
                        "username": username
                        }
                    self.commentRepo.create_comment_data(new_comment)
                    self.commentRepo.write_comments()
                    return "comment added"
                return "invalid username"
            return "invalid post_id"
        return "invalid message"

    def __validate_user(self, username):
        for users in self.userRepo.get_users():
            if users["username"] == username:
                return True
        return False

    def __validate_post(self, post_id):
        for posts in self.postRepo.get_posts():
            if posts["id"] == post_id:
                return True
        return False


    # def add_comment(self, message, post_id):
    #     self.all_comments = self.commentRepo.get_comments()
    #     comment_info = [str(uuid.uuid4()), post_id, self.user, message]
    #     self.all_comments.append(comment_info)
    #     self.commentRepo.write_comments(self.all_comments)



    #def edit_comment(self, message, commentID):
    #    self.all_comments = self.commentRepo.get_comments()
    #    comment_details = self.get_comment(commentID)
    #    for comments in self.all_comments:
    #        if comments[0] == comment_details[0]:
    #            comment_details[3] = message
    #    self.commentRepo.write_comments(self.all_comments)
#
    #def get_comment(self, commentID):
    #    self.all_comments = self.commentRepo.get_comments()
    #    for comments in self.all_comments:
    #        if comments[0] == commentID:
    #            return comments
#
    #def delete_by_username(self, username):
    #    self.all_comments = self.commentRepo.get_comments()
    #    index = 0
    #    for comments in self.all_comments:
    #        if comments[2] == username:
    #            self.all_comments.pop(index)
    #        index += 1
    #    self.commentRepo.write_comments(self.all_comments)
#
    #def delete(self, commentID):
    #    self.all_comments = self.commentRepo.get_comments()
    #    index = 0
    #    for comments in self.all_comments:
    #        if comments[0] == commentID:
    #            self.all_comments.pop(index)
    #        index += 1
    #    self.commentRepo.write_comments(self.all_comments)
#
    #def get_post_info(self, comment_id):
    #    self.all_comments = self.commentRepo.get_comments()
#
    #    for comments in self.all_comments:
    #        if comments[0] == comment_id:
    #            post = comments[1]
    #    all_posts = self.postRepo.get_allposts()
    #    for posts in all_posts:
    #        if posts[0] == post:
    #            return post
#
    #def get_comment_user_info(self, comment_id):
    #    self.all_comments = self.commentRepo.get_comments()
    #    for comments in self.all_comments:
    #        if comments[0] == comment_id:
    #            user = comments[2]
    #    all_users = self.userRepo.get_userlist()
    #    for users in all_users:
    #        if users[3] == user:
    #            return users
#
#


