from sys import path
path.append(".")
import json

class PostRepository:
    def __init__(self):
        self.post_data = [] 
 
    def get_posts(self):
        with open('src/data/posts.json', 'r') as json_data:
            self.post_data = json.loads(json_data.read())
            json_data.close()
        return self.post_data

    def write_posts(self):
        with open("src/data/posts.json", 'w') as json_data:
            json.dump(self.post_data, json_data, sort_keys=True, indent=4, separators=(',', ': '))
            json_data.close()
        return self.post_data
        

    def create_post_data(self, new_post):
        self.post_data.append(new_post)
        self.write_posts()
        return self.post_data

    def delete_post_data(self, index):
        self.post_data.pop(index)
        self.write_posts()
        return self.post_data
    
    def edit_post_data(self, post_list):
        self.post_data = post_list
        self.write_posts()