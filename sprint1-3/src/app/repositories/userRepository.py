import json

class UserRepository:
    def __init__(self):
        self.user_data = []
        self.get_users()

    def get_users(self):
        with open('src/data/users.json', 'r') as json_data:
            self.user_data = json.loads(json_data.read())
            json_data.close()
        return self.user_data

    def write_users(self):
        with open("src/data/users.json", 'w') as json_data:
            json.dump(self.user_data, json_data, sort_keys=True, indent=4, separators=(',', ': '))
            json_data.close()

    def create_user(self, new_user_info):
        self.user_data.append(new_user_info)
        self.write_users()
        return self.user_data

    def edit_user(self, user_list):
        self.user_data = user_list
        self.write_users()

    def delete_user(self, index):
        self.user_data.pop(index)
        self.write_users()
        return self.user_data