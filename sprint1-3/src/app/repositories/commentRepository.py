import csv
import json

class CommentRepository:
    def __init__(self):
        self.comment_data = []

    def get_comments(self):
        with open('src/data/comment.json', 'r') as json_data:
            self.comment_data = json.loads(json_data.read())
            json_data.close()
        return self.comment_data

    def write_comments(self):
        with open("src/data/comment.json", 'w') as json_data:
            json.dump(self.comment_data, json_data, sort_keys=True, indent=4, separators=(',', ': '))
            json_data.close()

    def create_comment_data(self, new_comment):
        self.comment_data.append(new_comment)
        return self.comment_data

    def delete_comment(self, index):
        self.comment_data.pop(index)
        self.write_comments()
        return self.comment_data
    