from sys import path
path.append(".")
import unittest
from src.app.repositories.commentRepository import CommentRepository
from src.app.service.commentService import CommentService

class Comment_Service_Test(unittest.TestCase):
    def setUp(self):
        self.instance = CommentRepository()
        self.service = CommentService()

    def test_get_comments(self):
        self.assertEqual(self.instance.get_comments(), self.service.get_all_comments())

    
    def test_write_comments(self):
        self.instance.write_comments()
        self.assertIsNot(self.instance.comment_data, None)
        self.assertEqual(type(self.instance.comment_data), list)

    def test_create_comment(self):
        length = len(self.instance.get_comments())
        comment_info = {
            "id": "8",
            "post_id": "1",
            "username": "Addi",
            "message": "Test create"
        }
        self.instance.create_comment_data(comment_info)
        self.instance.write_comments()
        self.assertEqual(length+1, len(self.instance.get_comments()))
        self.instance.delete_comment(length)
 
    def test_delete_comment(self):
        length = len(self.instance.get_comments())
        comment_info = {
            "id": "8",
            "post_id": "5",
            "username": "Addi",
            "message": "Test delete"
        }
        self.instance.create_comment_data(comment_info)
        self.instance.write_comments()
        self.assertNotEqual(length, len(self.instance.get_comments()))
        index = 0
        for comment in self.instance.get_comments():
            if comment["username"] == "Addi":
                pass
            index += 1
        self.instance.delete_comment(index-1)
        self.assertEqual(length, len(self.instance.get_comments()))

if __name__ == "__main__":
    unittest.main()