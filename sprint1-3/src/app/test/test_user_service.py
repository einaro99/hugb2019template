from sys import path
path.append(".")
import unittest
from src.app.service.userService import UserService


class User_Service_Test(unittest.TestCase):
    def setUp(self):
        self.instance = UserService()

    def test_username_exists(self):
        self.assertTrue(self.instance.username_exists("biggi"))#ertil
        self.assertFalse(self.instance.username_exists("ekkitil"))

    def test_isAdmin(self):
        self.assertTrue(self.instance.is_admin("nikki"))#eradmin
        self.assertFalse(self.instance.is_admin("jojosson"))#erekkiadmin

    def test_delete_user(self):
        self.instance.create_user("Test", "Temp", "1111111", "remove", "123", "temp@temp.is", False)
        length = len(self.instance.get_all_users())
        self.instance.delete_user("remove")
        one_less_length = len(self.instance.get_all_users())
        self.assertEqual(length-1, one_less_length)

    def test_create_user(self):
        length = len(self.instance.get_all_users())
        #print(length)
        self.instance.create_user("Test", "Temp", "1111111", "remove", "123", "temp@temp.is", False)
        one_more_length = len(self.instance.get_all_users())
        #print(one_more_length)
        self.instance.delete_user("remove")
        self.assertEqual(length+1, one_more_length)

    def test_get_user(self):
        wanted_user = {
        "email": "nikki@nikki.is",
        "first_name": "nikki",
        "isAdmin": True,
        "last_name": "nikkason",
        "password": "123",
        "phone": "1234567",
        "username": "nikki"
        }
        self.assertEqual(wanted_user, self.instance.get_user("nikki"))
        self.assertEqual(None, self.instance.get_user("jojosn"))
        print("testdone")

    def test_edit_user(self):
        wanted_user = {
        "email": "nikki@nikki.is",
        "first_name": "nikki",
        "isAdmin": True,
        "last_name": "nikkason",
        "password": "123",
        "phone": "1234567",
        "username": "nikki"
        }
        self.instance.create_user("nikki","nikkason","9999999","jonarsson","123456","siggi@gmail.is",True)
        self.instance.edit_user("1234567","nikki","nikki@nikki.is","123")
        self.assertEqual(wanted_user,self.instance.get_user("nikki"))
        self.instance.delete_user('jonarsson')


if __name__ == "__main__":
    unittest.main()