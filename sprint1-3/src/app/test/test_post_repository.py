from sys import path
path.append(".")
import unittest
from src.app.repositories.postRepository import PostRepository


class Post_Repository_Test(unittest.TestCase):
    def setUp(self):
        self.instance = PostRepository()

    def test_get_posts(self):
        self.assertTrue(len(self.instance.get_posts()) > 0)
    
    def test_get_posts2(self):
        self.instance.get_posts()
        self.assertEqual(len(self.instance.get_posts()), len(self.instance.post_data))
        self.assertEqual(type(self.instance.post_data), list)
        self.assertIsNot(self.instance.post_data, None)
    #something strange happens    
    def test_write_posts(self):
        self.instance.get_posts()
        self.instance.write_posts()
        self.assertIsNot(self.instance.post_data, None)
        self.assertEqual(self.instance.post_data, self.instance.write_posts())

    def test_create_post(self):
        length = len(self.instance.get_posts())
        post = {
        "id": "c3bf7ff0-51a3-4c96-9ac2-1cd445cb1f1a",
        "message": "test postur",
        "type": "Request",
        "username": "siggi"
    }
        self.instance.create_post_data(post)
        self.assertEqual(length+1, len(self.instance.get_posts()))
        self.instance.delete_post_data(length)

    def test_edit_post(self):
        length = len(self.instance.get_posts())
        all_posts = self.instance.get_posts()
        post = {
        "id": "2",
        "message": "BBBBaAA",
        "type": "Request",
        "username": "biggibiggi"
    }
        for posts in all_posts:
            if posts["id"] == post["id"]:
                posts["message"] = post["message"]
                posts["type"] = post["type"]
                posts["username"] = post["username"]
        self.instance.edit_post_data(all_posts)
        self.assertTrue(post in self.instance.get_posts())

    def test_delete_post(self):
        length = len(self.instance.get_posts())
        post = {
        "id": "c3bf7ff0-51a3-4c96-9ac2-1cd445cb1f1a",
        "message": "test postur",
        "type": "Request",
        "username": "siggi"
    }
        self.instance.create_post_data(post)
        self.assertNotEqual(length, len(self.instance.get_posts()))
        self.instance.delete_post_data(len(self.instance.get_posts())-1)
        self.assertEqual(length, len(self.instance.get_posts()))

if __name__ == "__main__":
    unittest.main()
