from sys import path
path.append(".")
import unittest
from src.app.service.postService import PostService
from src.app.repositories.postRepository import PostRepository
import uuid




class User_Service_Test(unittest.TestCase):
    def setUp(self):
        self.instance = PostService()
        self.repo = PostRepository() 

    def test_post(self):
        self.assertEqual(self.instance.get_all_posts(), self.repo.get_posts())

    def test_delete(self):
        #Creates a new user in the back of the list and deletes the user

        new_post = {
            "id": "1234",
            "message": "This is a test",
            "type": "Request",
            "user": "rikki"

        }
        self.instance.add_post(new_post["message"], new_post["type"], new_post["user"])
        length_before = len(self.instance.get_all_posts())
        self.instance.delete_post(self.instance.get_all_posts()[-1]["id"])
        length_after = len(self.instance.get_all_posts())
        self.assertEqual(length_before -1, length_after)


    def test_put(self):
        value = str(uuid.uuid4())
        new_post = {
            "id": "123123",
            "message": "This is a test",
            "type": "Request",
            "user": "rikki"
        }
        self.instance.add_post(new_post["message"], new_post["type"], new_post["user"])
        self.instance.edit_post(value, self.instance.get_all_posts()[-1]["id"],"","")
        self.assertEqual(value, self.instance.get_all_posts()[-1]["message"])
        self.instance.delete_post(self.instance.get_all_posts()[-1]["id"])

    def test_get_post(self):
        post_id = "2"
        wanted_post = {
            "id": "2",
            "message": "BBBBaAA", 
            "type": "Request",
            "username": "biggibiggi"
            }
        self.assertEqual(wanted_post, self.instance.get_post_id(post_id))

    def test_get_user_posts(self):
        user_posts = [{"id": "c8c8a70d-e395-45f3-97ab-a613574ea218", "message": "Slátturvél","type": "Request","username": "biggi"}]
        self.assertEqual(user_posts, self.instance.get_user_posts("biggi"))
    #
if __name__ == "__main__":
    unittest.main()