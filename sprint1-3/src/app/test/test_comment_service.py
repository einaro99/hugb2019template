from sys import path
path.append(".")
import unittest
from src.app.service.commentService import CommentService
from src.app.repositories.commentRepository import CommentRepository
import uuid

class Comment_Service_Test(unittest.TestCase):
    def setUp(self):
        self.instance = CommentService()
        self.repo = CommentRepository()

    def test_edit_comment(self):
        old_comment = self.instance.get_comment("10")
        edit_comment = {
            "id": "10",
            "message": "After Change"
        }
        self.instance.edit_comment(edit_comment["id"], edit_comment["message"])
        # self.assertTrue(edit_comment in self.repo.get_comments())
        self.assertNotEqual(old_comment["message"], edit_comment["message"])
        
        
    def test_get_comment(self):
        comment_id = "1"
        wanted_comment = {
            "id": "1",
            "post_id": "1",
            "username": "Addi",
            "message": "test 1"
        }
        self.assertEqual(wanted_comment, self.instance.get_comment(comment_id))

    def test_get_post_comment(self):
        comment_post = [{"id": "1", "post_id": "1", "username": "Addi", "message": "test 1"}, {"id": "2", "post_id": "1", "username": "Addi", "message": "test 1 nmr 2"}]
        self.assertEqual(comment_post, self.instance.get_post_comments("1"))

    def test_get_priv_post_comments(self):
        priv_comment_post = [{"id": "8", "post_id": "6", "username": "Addi", "message": "test priv comment"}, {"id": "9", "post_id": "6", "username": "Arnthor", "message": "test priv comment2"}]
        self.assertEqual(priv_comment_post, self.instance.get_priv_post_comments("6", "Addi", "Arnthor"))

    def test_get_all_comments(self):
        self.assertEqual(self.instance.get_all_comments(), self.repo.get_comments())

    def test_delete_comment(self):
        length_before = len(self.instance.get_all_comments())
        new_comment = {
            "post_id": "2",
            "username": "Addi",
            "message": "test delete comment"
        }
        self.instance.create_comment(new_comment["message"], new_comment["post_id"], new_comment["username"])
        length_after = len(self.instance.get_all_comments())
        self.assertEqual(length_before +1, length_after)
        self.instance.delete_comment(self.instance.get_all_comments()[-1]["id"])

    def test_create_comment(self):
        length = len(self.instance.get_all_comments())
        new_comment = {
            "post_id": "2",
            "username": "Addi",
            "message": "test service"
        }
        self.instance.create_comment(new_comment["message"], new_comment["post_id"], new_comment["username"])
        self.assertEqual(length+1, len(self.instance.get_all_comments()))
        self.instance.delete_comment(self.instance.get_all_comments()[-1]["id"])

if __name__ == "__main__":
    unittest.main()


