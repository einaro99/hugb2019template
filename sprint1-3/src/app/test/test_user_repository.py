from sys import path
path.append(".")
import unittest
from src.app.repositories.userRepository import UserRepository
from src.app.service.userService import UserService


class User_Service_Test(unittest.TestCase):
    def setUp(self):
        self.instance = UserRepository()
        self.User_serv = UserService()

    def test_get_users(self):
        self.instance.get_users()
        self.assertEqual(len(self.instance.get_users()), len(self.instance.user_data))
        self.assertIsNot(self.instance.user_data, None)

    def test_write_users(self):
        self.instance.write_users()
        self.assertIsNot(self.instance.user_data, None)

    def test_create_user(self):
        length = len(self.instance.get_users())
        user_info = {
            "email": "temp@temp.is",
            "first_name": "Test",
            "isAdmin": False,
            "last_name": "Temp",
            "password": "123",
            "phone": "1234567890",
            "username": "remove"
        }
        self.instance.create_user(user_info)
        self.assertEqual(length+1, len(self.instance.get_users()))
        self.instance.delete_user(length)

    def test_edit_user(self):
        #Creates a new list users and creates a new user with user info and then removes that user and appends the updates users info
        #Deletes the temp user info after testing.
        users = self.instance.get_users()
        user_info = {
            "email": "temp@temp.is",
            "first_name": "Test",
            "isAdmin": False,
            "last_name": "Temp",
            "password": "123",
            "phone": "1234567890",
            "username": "remove"
        }
        users.append(user_info)
        updated_user_info = {
            "email": "tempt2@temp.is",
            "first_name": "Test2",
            "isAdmin": False,
            "last_name": "Temp2",
            "password": "321",
            "phone": "0987654321",
            "username": "remove2"
        }
        users.pop(3)
        users.append(updated_user_info)
        #Edit_user(updated_user_list)requires a new list with the updated info
        self.instance.edit_user(users)
        self.assertIsNot(updated_user_info, self.User_serv.get_user("remove2"))
        index = 0
        for user in self.User_serv.all_users:
            if user["username"] == "remove2":
                pass
            index += 1
        self.instance.delete_user(index)

    def test_delete_user(self):
        user_info = {
            "email": "temp@temp.is",
            "first_name": "Test",
            "isAdmin": False,
            "last_name": "Temp",
            "password": "123",
            "phone": "1234567890",
            "username": "remove"
        }
        length = len(self.instance.get_users())
        self.instance.create_user(user_info)
        self.assertNotEqual(length, len(self.instance.get_users()))
        index = 0
        for user in self.User_serv.all_users:
            if user["username"] == "remove":
                pass
            index += 1
        self.instance.delete_user(index)
        self.assertEqual(length, len(self.instance.get_users()))


if __name__ == "__main__":
    unittest.main()